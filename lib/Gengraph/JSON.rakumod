unit class Gengraph::JSON;

use JSON::Fast;

has Hash $.config = %(
    nodes         => 10,
    conditions    => 4,
    condchain_len => 2,
    node_template => %(
        content => 1,
        type    => "f",
        args    => %()
    )
);
has List $.funcs = (
    %(1  => 'email', args => %(pa_id => 'rand_uint')),
    %(2  => 'SMS', args => %(pa_id => 'rand_uint')),
    %(3  => 'wait', args => %(duration => 'rand_uint')),
    %(4  => 'payment_status', args => %(po_id => 'rand_uint')),
    %(5  => 'increase_payment', args => %(daysactive => 'rand_uint')),
    %(6  => 'cancel_payment', args => %(po_id => 'rand_uint')),
    %(7  => 'send_debt_collector', args => %(daysactive => 'Default debt collector')),
    %(8  => 'order', args => %(po_id => 'rand_uint')),
    %(9  => 'set_order_status', args => %(status => 'rand_status')),
    %(10 => 'set_status', args => %(status => 'rand_status')),
    %(11 => 'email_delivery_status', args => %(status => 'rand_status')),
    %(12 => 'sms_response', args => %(status => 'rand_status')),
    %(13 => 'letter', args => %(pa_id => 'rand_uint')),
);

constant statuses = ['new', 'initialized', 'cancelled', 'completed', 'fraud', 'queue'];
constant conditions = [ '=', '!=', '<', '>', '<=', '>=', 'and', 'or', 'not', 'in'];
constant chainglue  = [ 'and', 'or' ];


method generate_nodes returns Hash {
    my Hash $ret = %(nodes => %());
    my @cond_nodes = self.get_condition_nodes;
    for ^$!config<nodes> {
        my UInt $nodid = $_ + 2;
        my UInt $index = $!funcs.elems.rand.UInt;

        my %args = ($!funcs[$index])<args>;
        %args.keys.map({ %args{$_} = 1000.rand.UInt if %args{$_} eq 'rand_uint' });
        %args.keys.map({ %args{$_} = statuses.pick if %args{$_} eq 'rand_status' });

        if @cond_nodes.map({$_ if $_ == ($nodid - 2)}) {
            my @chain;
            my $cond_len = $!config<condchain_len>.rand.UInt + 1;
            for ^$cond_len {
                @chain.push(
                    q{(} ~ <tab.x tab.y tab.z>.pick ~ q{ } ~ conditions.pick ~ q{ } ~ <shchema.k shchema.m shchema.n shchema.t>.pick ~ q{)}
                );
                @chain.push(chainglue.pick) unless $_ >= ($cond_len - 1);
            }
            %args<condition> = @chain.join(q{ });
        }

        $ret<nodes>{$nodid} = %(
            content => $index + 1,
            type    => 'f',
            args    => %args
        );
    }
    $ret;
}

method generate_edges(:%nodes) returns Hash {
    my Hash $ret = %(edges => %());
    for ^$!config<nodes> {
        my UInt $edgeid = $_ + 1;
        $ret<edges>{$edgeid}.push(%(target => $_ + 2));
        if (%nodes<nodes>{$_ + 1}<args><condition>:exists) && %nodes<nodes>{$_ + 1}<args><condition> ne q{} {
            $ret<edges>{$edgeid}.push(%(target => $_ + 3, condition => %nodes<nodes>{$_ + 1}<args><condition>));
        }
    }
    $ret;
}

method get_condition_nodes returns Array {
    my @ret;
    while (
        (@ret.elems < $!config<conditions>) &&
        ($!config<conditions> <= ($!config<nodes> - 2))
    ) {
        my $node = ($!config<nodes> - 2).rand.UInt;
        @ret.push($node) unless @ret.map({$_ if $_ == $node});
    }
    @ret;
}

method json(%data) returns Str {
    to-json(%data, :sorted-keys);
}

method json-to-file(%data, Str $fname) returns Bool {
    $fname.IO.spurt(to-json(%data, :sorted-keys));
}
