use Test;
use Gengraph::JSON;

plan 2;

my Bool $subtest1 = subtest(
    'Generate graph',
    sub {
        plan 3;
        my $gg = Gengraph::JSON.new;
        my $nodes = $gg.generate_nodes;
        my $edges = $gg.generate_edges(:nodes($nodes));

        ok $nodes, 'generate_nodes';
        is $gg.get_condition_nodes.elems, $gg.config<conditions>, 'get_condition_nodes';

        # diag($gg.json(%(|$nodes, |$edges)));
        ok $gg.json-to-file(%(|$nodes, |$edges), 'test-graph.json'), 'save json to file';
    }
).Bool;

my Bool $subtest2 = subtest(
    'Check invalid config values',
    sub {
        plan 4;
        my $gg;
        my Hash $cfg;
        $cfg = %(
            nodes         => 2,
            conditions    => 2,
            condchain_len => 2,
            node_template => %(
                content => 1,
                type    => "f",
                args    => %()
            ),
        );
        $gg = Gengraph::JSON.new(:config($cfg));
        nok $gg.get_condition_nodes.elems, 'get_condition_nodes returns empty';

        $cfg = %(
            nodes         => 2,
            conditions    => 3,
            condchain_len => 2,
            node_template => %(
                content => 1,
                type    => "f",
                args    => %()
            ),
        );
        $gg = Gengraph::JSON.new(:config($cfg));
        nok $gg.get_condition_nodes.elems, 'get_condition_nodes returns empty';

        $cfg = %(
            nodes         => 3,
            conditions    => 2,
            condchain_len => 2,
            node_template => %(
                content => 1,
                type    => "f",
                args    => %()
            ),
        );
        $gg = Gengraph::JSON.new(:config($cfg));
        nok $gg.get_condition_nodes.elems, 'get_condition_nodes returns empty';

        $cfg = %(
            nodes         => 3,
            conditions    => 1,
            condchain_len => 2,
            node_template => %(
                content => 1,
                type    => "f",
                args    => %()
            ),
        );
        $gg = Gengraph::JSON.new(:config($cfg));
        is $gg.get_condition_nodes.elems, 1, 'get_condition_nodes returns one item array';
    }
).Bool;
