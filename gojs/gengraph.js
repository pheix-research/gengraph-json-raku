var CodeMirror;

function init() {
    if (window.goSamples) goSamples(); // init for these samples -- you don't need to call this
    var $ = go.GraphObject.make; // for conciseness in defining templates

    // some constants that will be reused within templates
    var roundedRectangleParams = {
        parameter1: 2, // set the rounded corner
        spot1: go.Spot.TopLeft,
        spot2: go.Spot.BottomRight // make content go all the way to inside edges of rounded corners
    };

    myDiagram =
        $(go.Diagram, "myDiagramDiv", // must name or refer to the DIV HTML element
            {
                "animationManager.initialAnimationStyle": go.AnimationManager.None,
                "InitialAnimationStarting": function(e) {
                    var animation = e.subject.defaultAnimation;
                    animation.easing = go.Animation.EaseOutExpo;
                    animation.duration = 900;
                    animation.add(e.diagram, 'scale', 0.1, 1);
                    animation.add(e.diagram, 'opacity', 0, 1);
                },

                // have mouse wheel events zoom in and out instead of scroll up and down
                "toolManager.mouseWheelBehavior": go.ToolManager.WheelZoom,
                // support double-click in background creating a new node
                "clickCreatingTool.archetypeNodeData": {
                    text: "new node"
                },
                // enable undo & redo
                "undoManager.isEnabled": true,
                positionComputation: function(diagram, pt) {
                    return new go.Point(Math.floor(pt.x), Math.floor(pt.y));
                }
            });

    // define the Node template
    myDiagram.nodeTemplate =
        $(go.Node, "Auto", {
                locationSpot: go.Spot.Top,
                isShadowed: true,
                shadowBlur: 1,
                shadowOffset: new go.Point(0, 1),
                shadowColor: "rgba(0, 0, 0, .14)"
            },
            new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
            // define the node's outer shape, which will surround the TextBlock
            $(go.Shape, "RoundedRectangle", roundedRectangleParams, {
                name: "SHAPE",
                fill: "#ffffff",
                strokeWidth: 0,
                stroke: null,
                portId: "", // this Shape is the Node's port, not the whole Node
                fromLinkable: true,
                fromLinkableSelfNode: true,
                fromLinkableDuplicates: true,
                toLinkable: true,
                toLinkableSelfNode: true,
                toLinkableDuplicates: true,
                cursor: "pointer"
            }),
            $(go.TextBlock, {
                    font: "bold small-caps 11pt helvetica, bold arial, sans-serif",
                    margin: 7,
                    stroke: "rgba(0, 0, 0, .87)",
                    editable: true // editing the text automatically updates the model data
                },
                new go.Binding("text").makeTwoWay())
        );


    // unlike the normal selection Adornment, this one includes a Button
    myDiagram.nodeTemplate.selectionAdornmentTemplate =
        $(go.Adornment, "Spot",
            $(go.Panel, "Auto",
                $(go.Shape, "RoundedRectangle", roundedRectangleParams, {
                    fill: null,
                    stroke: "#7986cb",
                    strokeWidth: 3
                }),
                $(go.Placeholder) // a Placeholder sizes itself to the selected Node
            ),
            // the button to create a "next" node, at the top-right corner
            $("Button", {
                    alignment: go.Spot.TopRight,
                    click: addNodeAndLink // this function is defined below
                },
                $(go.Shape, "PlusLine", {
                    width: 6,
                    height: 6
                })
            ) // end button
        ); // end Adornment

    myDiagram.nodeTemplateMap.add("Start",
        $(go.Node, "Spot", {
                desiredSize: new go.Size(75, 75)
            },
            new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
            $(go.Shape, "Circle", {
                fill: "#52ce60",
                /* green */
                stroke: null,
                portId: "",
                fromLinkable: true,
                fromLinkableSelfNode: true,
                fromLinkableDuplicates: true,
                toLinkable: true,
                toLinkableSelfNode: true,
                toLinkableDuplicates: true,
                cursor: "pointer"
            }),
            $(go.TextBlock, "Start", {
                font: "bold 16pt helvetica, bold arial, sans-serif",
                stroke: "whitesmoke"
            })
        )
    );

    myDiagram.nodeTemplateMap.add("End",
        $(go.Node, "Spot", {
                desiredSize: new go.Size(75, 75)
            },
            new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
            $(go.Shape, "Circle", {
                fill: "maroon",
                stroke: null,
                portId: "",
                fromLinkable: true,
                fromLinkableSelfNode: true,
                fromLinkableDuplicates: true,
                toLinkable: true,
                toLinkableSelfNode: true,
                toLinkableDuplicates: true,
                cursor: "pointer"
            }),
            $(go.Shape, "Circle", {
                fill: null,
                desiredSize: new go.Size(65, 65),
                strokeWidth: 2,
                stroke: "whitesmoke"
            }),
            $(go.TextBlock, "End", {
                font: "bold 16pt helvetica, bold arial, sans-serif",
                stroke: "whitesmoke"
            })
        )
    );

    // clicking the button inserts a new node to the right of the selected node,
    // and adds a link to that new node
    function addNodeAndLink(e, obj) {
        var adornment = obj.part;
        var diagram = e.diagram;
        diagram.startTransaction("Add State");

        // get the node data for which the user clicked the button
        var fromNode = adornment.adornedPart;
        var fromData = fromNode.data;
        // create a new "State" data object, positioned off to the right of the adorned Node
        var toData = {
            text: "new"
        };
        var p = fromNode.location.copy();
        p.x += 200;
        toData.loc = go.Point.stringify(p); // the "loc" property is a string, not a Point object
        // add the new node data to the model
        var model = diagram.model;
        model.addNodeData(toData);

        // create a link data from the old node data to the new node data
        var linkdata = {
            from: model.getKeyForNodeData(fromData), // or just: fromData.id
            to: model.getKeyForNodeData(toData),
            text: "transition"
        };
        // and add the link data to the model
        model.addLinkData(linkdata);

        // select the new Node
        var newnode = diagram.findNodeForData(toData);
        diagram.select(newnode);

        diagram.commitTransaction("Add State");

        // if the new node is off-screen, scroll the diagram to show the new node
        diagram.scrollToRect(newnode.actualBounds);
    }

    // replace the default Link template in the linkTemplateMap
    myDiagram.linkTemplate =
        $(go.Link, // the whole link panel
            {
                curve: go.Link.Bezier,
                adjusting: go.Link.Stretch,
                reshapable: false,
                relinkableFrom: false,
                relinkableTo: false,
                toShortLength: 3
            },
            new go.Binding("points").makeTwoWay(),
            new go.Binding("curviness"),
            $(go.Shape, // the link shape
                {
                    strokeWidth: 1.5
                },
                new go.Binding('stroke', 'progress', function(progress) {
                    return progress ? "#52ce60" /* green */ : 'black';
                }),
                new go.Binding('strokeWidth', 'progress', function(progress) {
                    return progress ? 2.5 : 1.5;
                })
            ),
            $(go.Shape, // the arrowhead
                {
                    toArrow: "standard",
                    stroke: null
                },
                new go.Binding('fill', 'progress', function(progress) {
                    return progress ? "#52ce60" /* green */ : 'black';
                })),
            $(go.Panel, "Auto",

                $(go.TextBlock, "transition", // the label text
                    {
                        textAlign: "center",
                        font: "9pt helvetica, arial, sans-serif",
                        margin: 4,
                        editable: true // enable in-place editing
                    },
                    // editing the text automatically updates the model data
                    new go.Binding("text").makeTwoWay())
            )
        );

    // read in the JSON data from the "mySavedModel" element
    //load();
}

// Show the diagram's model in JSON format
function save() {
    document.getElementById("mySavedModel").value = myDiagram.model.toJson();
}

function load(gomodel) {
    //myDiagram.model = go.Model.fromJson(document.getElementById("mySavedModel").value);
    myDiagram.model = go.Model.fromJson(gomodel);
}

function getGeneratedJSON() {
    var nodes = $('#NodesNum').val();
    var conds = $('#ConditionsNum').val();
    var chain = $('#ChainLen').val();
    var url   = 'https://gengraph-31.dev/generate/?nodes=' + nodes + '&conditions=' + conds + '&condchainl=' + chain;
    console.log(url);
    $.getJSON(url, function(data) {
        //console.log(data);
        //load(JSON.stringify(data));
        CodeMirror.setValue(JSON.stringify(data, null, 2));
        convert();
    });
}

function getXMLApiJSON() {
    var url = 'https://gengraph-31.dev/xml/';
    console.log(url);
    $.post(url, $("#fetchJSONForm").serialize(), function(data, textStatus) {
        CodeMirror.setValue(JSON.stringify(data, null, 2));
        convert();
    }, "json");
    $('#exampleModal').modal('hide');
}

var funcs = {
    "1": 'email',
    "2": 'SMS',
    "3": 'wait',
    "4": 'payment_status',
    "5": 'increase_payment',
    "6": 'cancel_payment',
    "7": 'send_debt_collector',
    "8": 'order',
    "9": 'set_order_status',
    "10": 'set_status',
    "11": 'email_delivery_status',
    "12": 'sms_response',
    "13": 'letter',
    "17": 'prepayment'
};

var generated_graph = '{"edges":{"1":[{"target":2},{"condition":"(tab.x <= shchema.m) or (tab.y < shchema.k)","target":3}],"2":[{"target":3}],"3":[{"target":4}],"4":[{"target":5}]},"nodes":{"2":{"args":{"condition":"(tab.x <= shchema.m) or (tab.y < shchema.k)","pa_id":92},"content":2,"type":"f"},"3":{"args":{"pa_id":875},"content":1,"type":"f"},"4":{"args":{"status":"queue"},"content":12,"type":"f"},"5":{"args":{"po_id":336},"content":6,"type":"f"}}}';

function get_multiple_from(bulk) {
    var dupl = [];
    for (edgeindex in bulk.linkDataArray) {
        //console.log(bulk.linkDataArray[edgeindex]);
        var from = bulk.linkDataArray[edgeindex].from;
        var existed = false;
        for(var i = 0; i < dupl.length; i++) {
            if (dupl[i][from] >= 1) {
                //console.log(from + ":" + dupl[i][from] + ", " + i);
                dupl[i][from]++;
                existed = true;
            }
        }
        if (!existed) {
            var obj = {};
            obj[from] = 1;
            dupl.push(obj);
            //console.log('pushed ' + from);
        }
    }
    //console.log(dupl);
    for (nodeindex in bulk.nodeDataArray) {
        var node = bulk.nodeDataArray[nodeindex];
        for(var i = 0; i < dupl.length; i++) {
            if (dupl[i][node.id] > 1) {
                console.log('found multiple edge immiter node ' + node.id);
                //node.loc = 0 + " " + 100 * nodeindex;
            }
        }

    }
    return dupl;
    //Object.keys(bulk.edges).forEach(function(key) {
}

function get_multiple_to(bulk) {
    var dupl = [];
    for (edgeindex in bulk.linkDataArray) {
        //console.log(bulk.linkDataArray[edgeindex]);
        var to = bulk.linkDataArray[edgeindex].to;
        var existed = false;
        for(var i = 0; i < dupl.length; i++) {
            if (dupl[i][to] >= 1) {
                //console.log(to + ":" + dupl[i][to] + ", " + i);
                dupl[i][to]++;
                existed = true;
            }
        }
        if (!existed) {
            var obj = {};
            obj[to] = 1;
            dupl.push(obj);
            //console.log('pushed ' + from);
        }
    }
    //console.log(dupl);
    for (nodeindex in bulk.nodeDataArray) {
        var node = bulk.nodeDataArray[nodeindex];
        for(var i = 0; i < dupl.length; i++) {
            if (dupl[i][node.id] > 1) {
                console.log('found multiple edge accessor node ' + node.id);
                //node.loc = 0 + " " + 100 * nodeindex;
            }
        }

    }
    return dupl;
}

function convert() {
    var bulk = {
        "class": "go.GraphLinksModel",
        "nodeKeyProperty": "id",
        "nodeDataArray": [{
            "id": -1,
            "loc": "0 0",
            "category": "Start",
            "fixed": false,
            "text": "start node"
        }],
        "linkDataArray": []
    };
    //var g = JSON.parse(document.getElementById('mySavedModel').value);
    var g = JSON.parse(CodeMirror.getValue());
    //console.log(g);

    Object.keys(g.nodes).forEach(function(key) {
        var fid;
        var txt;
        var comment = '';
        var loc = 0 + " " + 100 * key;
        if (g.nodes[key].type === "f") {
            fid = parseInt(g.nodes[key].content);
            txt = funcs[fid]; // + "(" + g.nodes[key].type + "_" + g.nodes[key].content + ")";
        }
        else {
            txt = parseInt(g.nodes[key].content);
        }
        //console.log(txt);
        if (g.nodes[key].comment) {
            comment = "(" + g.nodes[key].comment + ")";
        }
        bulk.nodeDataArray.push({
            "id": key,
            "loc": loc,
            "text": (key + ":" + txt + comment),
            "fixed": false
        });
    });

    Object.keys(g.edges).forEach(function(key) {
        var nodeid = parseInt(key) + 1;
        var node = parseInt(key) == 1 ? -1 : parseInt(key);

        var edge_len = "edge " + key + ":";

        for (target in g.edges[key]) {
            var progress = false;
            var to = g.edges[key][target].target;
            //console.log("from " + node + " to " + to);
            var cond = '';
            if (g.edges[key][target].condition) {
                cond = g.edges[key][target].condition;
                progress = true;
            }
            /*if (target > 0) {
                bulk.nodeDataArray[to - 1].loc = -200 * target + " " + (100 * (to - target) + 50 * (g.edges[key].length - target));
            }*/
            loc = (200 * target) + " " + (100 * nodeid);
            if (!bulk.nodeDataArray[to - 1].fixed) {
                //console.log("target=" + target + " id=" + bulk.nodeDataArray[to - 1].id + " loc=" + loc);
                bulk.nodeDataArray[to - 1].loc = loc;
            }
            bulk.nodeDataArray[to - 1].fixed = true;
            bulk.linkDataArray.push({
                "from": node,
                "to": to,
                "text": cond,
                "progress": progress
            });
        }
        //node = nodeid;
    });
    //document.getElementById("mySavedModel").value = JSON.stringify(bulk);
    //rearrange(bulk);
    load(JSON.stringify(bulk));
}
