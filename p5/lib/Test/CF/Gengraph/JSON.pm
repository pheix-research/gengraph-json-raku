package Test::CF::Gengraph::JSON;

use standards -class => 1;
use types [qw/Invocant Str Bool HashRef ArrayRef/];
use feature 'say';

use Data::Dumper;
use Encode qw(decode);
use File::Slurp qw(write_file);
use JSON::PP;

use constant statuses   => ['new', 'initialized', 'cancelled', 'completed', 'fraud', 'queue'];
use constant conditions => ['=',   '!=',          '<',         '>',         '<=',    '>=', 'and', 'or', 'not', 'in'];
use constant chainglue => ['and',       'or'];
use constant c01       => ['tab.x',     'tab.y', 'tab.z'];
use constant c02       => ['shchema.k', 'shchema.m', 'shchema.n', 'shchema.t'];

has 'config',
  is      => 'rw',
  isa     => HashRef,
  default => sub {
    {
        nodes         => 10,
        conditions    => 4,
        condchain_len => 2,
        node_template => {
            content => 1,
            type    => "f",
            args    => {}
        }
    };
  },
  required => 1;

has 'funcs',
  is      => 'rw',
  isa     => ArrayRef,
  default => sub {
    my @a = (
        {1  => 'email',                 args => {pa_id      => 'rand_uint'}},
        {2  => 'SMS',                   args => {pa_id      => 'rand_uint'}},
        {3  => 'wait',                  args => {duration   => 'rand_uint'}},
        {4  => 'payment_status',        args => {po_id      => 'rand_uint'}},
        {5  => 'increase_payment',      args => {daysactive => 'rand_uint'}},
        {6  => 'cancel_payment',        args => {po_id      => 'rand_uint'}},
        {7  => 'send_debt_collector',   args => {daysactive => 'Default debt collector'}},
        {8  => 'order',                 args => {po_id      => 'rand_uint'}},
        {9  => 'set_order_status',      args => {status     => 'rand_status'}},
        {10 => 'set_status',            args => {status     => 'rand_status'}},
        {11 => 'email_delivery_status', args => {status     => 'rand_status'}},
        {12 => 'sms_response',          args => {status     => 'rand_status'}},
        {13 => 'letter',                args => {pa_id      => 'rand_uint'}},
    );
    \@a;
  },
  required => 1;

sub generate_nodes : Requires(Invocant) Returns(HashRef) {
    my $self = shift;

    my $ret = {nodes => {}};

    my $cond_nodes = $self->get_condition_nodes();

    for my $i (0 .. $self->config->{nodes}) {
        my $nodid = $i + 2;
        my $index = int(rand(scalar @{$self->funcs}));

        my $args = (@{$self->funcs})[$index]->{args};

        map { $args->{$_} = int(rand(1000))         if $args->{$_} eq 'rand_uint' } keys %{$args};
        map { $args->{$_} = $self->_pick(+statuses) if $args->{$_} eq 'rand_status' } keys %{$args};

        if (scalar map { $_ } grep { $_ == ($nodid - 2) } @{$cond_nodes}) {
            my @chain;
            my $cond_len = int(rand($self->config->{condchain_len})) + 1;
            for my $j (0 .. $cond_len) {
                push @chain,
                  q{(} . $self->_pick(+c01) . q{ } . $self->_pick(+conditions) . q{ } . $self->_pick(+c02) . q{)};
                push @chain, $self->_pick(+chainglue) unless $j >= ($cond_len - 1);
            }
            $args->{condition} = join q{ }, @chain;
        }

        $ret->{nodes}->{$nodid} = {
            content => $index + 1,
            type    => 'f',
            args    => $args
        };
    }

    return $ret;
}

sub generate_edges : Requires(Invocant, HashRef) Returns(HashRef) {
    my ($self, $nodes) = @_;

    my $ret = {edges => {}};

    for my $i (0 .. $self->config->{nodes}) {
        my $edgeid = $i + 1;
        my $nodeid = $i + 2;

        push @{$ret->{edges}->{$edgeid}}, {target => $i + 2};

        if (defined $nodes->{nodes}->{$nodeid}->{args}->{condition}
            && $nodes->{nodes}->{$nodeid}->{args}->{condition} ne q{})
        {
            push @{$ret->{edges}->{$edgeid}},
              {target => $i + 3, condition => $nodes->{nodes}->{$nodeid}->{args}->{condition}};
        }
    }

    return $ret;
}

sub get_condition_nodes : Requires(Invocant) Returns(ArrayRef) {
    my $self = shift;

    my @ret;

    while ((scalar @ret < $self->config->{conditions})
        && ($self->config->{conditions} <= ($self->config->{nodes} - 2)))
    {
        my $node = int(rand($self->config->{nodes} - 2));
        push @ret, $node unless scalar map { $_ } grep { $_ == $node } @ret;
    }

    return \@ret;
}

sub json : Requires(Invocant, HashRef) Returns(Str) {
    my ($self, $data) = @_;

    my $coder = JSON::PP->new->pretty->allow_nonref;

    return $coder->sort_by(
        sub {
            ($JSON::PP::a =~ /^[\d]+$/ && $JSON::PP::b =~ /^[\d]+$/ && $JSON::PP::a <=> $JSON::PP::b)
              || ($JSON::PP::a cmp $JSON::PP::b);
        }
    )->encode($data);
}

sub json_to_file : Requires(Invocant, Str, HashRef) Returns(Bool) {
    my ($self, $fname, $data) = @_;

    write_file($fname, $self->json($data) // '{}');

    return 1;
}

sub _pick : Requires(Invocant, ArrayRef) Returns(Str) {
    my ($self, $data) = @_;

    return @{$data}[int(rand(scalar @{$data}))];
}

1;

__END__

=head1 NAME

Test::CF::Gengraph::JSON - generic helper component for Collecting Flows v2 (CFv2) tests. It allows sample CFv2 graph generation.

=head1 SYNOPSYS

    use Test::CF::Gengraph::JSON;
    my $gg    = Test::CF::Gengraph::JSON->new();
    my $nodes = $gg->generate_nodes();
    my $edges = $gg->generate_edges($nodes);
    $gg.json-to-file({ %$nodes, %$edges }, './test-graph.json');

=head1 DESCRIPTION

Test::CF::Gengraph::JSON - generic helper component for Collecting Flows v2 (CFv2) tests. It allows sample CFv2 graph generation.

=head1 PROPERTIES

=over 4

=item config

Initial configuration.

=item func

Set of CFv2 functions.

=back

=head1 METHODS

=over 4

=item generate_nodes()

Generates set of nodes.

=item generate_edges($nodes)

Generates set of edges based on previously generated nodes given by $nodes hash reference.

=item get_condition_nodes()

Gets nodes with conditions.

=item json($data)

Generate JSON on input $data hash reference.

=item json-to-file($data, $fname)

Generate JSON on input $data hash reference and saves it to file $fname.

=back

=cut
