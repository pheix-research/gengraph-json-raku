use strict;
use warnings;
use feature 'say';

BEGIN { our $start_run = time(); }

use Data::Dumper;
use Encode;
use File::Slurp qw(read_file);
use JSON::XS qw(decode_json);
use Test::MonkeyMock;
use Time::HiRes qw(time);

use MSP::CF::Workflow::Graph;

die "no filename given in command line" unless defined $ARGV[0];

use constant filename  => $ARGV[0] // q{};
use constant strgraph  => -e -f filename ? encode('utf8', read_file(filename)) : '{}';
use constant jsongraph => decode_json(strgraph);

my $dbh = Test::MonkeyMock->new;
$dbh->mock(selectall_hashref => sub { {tab => {f_id => 990}, shchema => {f_id => 991} } });

my $g = MSP::CF::Workflow::Graph->new(
    start => 1,
    data  => {1 => strgraph},
    dbh   => $dbh,
);

say 'Graph details: nodes=' . (keys %{ (&jsongraph)->{nodes} }) . ', edges=' . (keys %{ (&jsongraph)->{nodes} });
say join "\n", $g->error unless $g->validate;
say $g->{_valid} ? 'Graph is valid' : 'Graph is invalid';
say 'Job took ' . (time() - our $start_run) . 'seconds';
