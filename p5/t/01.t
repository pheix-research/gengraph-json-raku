use standards -test => 1;

use Test::Deep;
use Test::Env;
use Test::TempDir::Tiny;
use JSON::PP;
use File::Spec;
use File::Slurp qw(read_file);

use Test::CF::Gengraph::JSON;

subtest 'graph generation' => sub {
    my $gg    = Test::CF::Gengraph::JSON->new();
    my $coder = JSON::PP->new->pretty->allow_nonref;
    my $path  = File::Spec->catfile(tempdir(), 'json-test.json');

    my $cn    = $gg->get_condition_nodes();
    my $nodes = $gg->generate_nodes();
    my $edges = $gg->generate_edges($nodes);
    $gg->json_to_file($path, {%$nodes, %$edges});

    is scalar @{$cn}, $gg->config->{conditions}, 'generate indexes of conditional nodes';

    ok defined $nodes, 'generate sample set of nodes';

    ok defined $edges, 'generate sample set of edges';

    cmp_deeply($coder->decode($gg->json({%$nodes, %$edges})), {%$nodes, %$edges}, "check decoded JSON from string");

    ok -f $path, "JSON data saved to file";

    my $json_str = read_file($path);

    cmp_deeply($coder->decode($json_str), {%$nodes, %$edges}, "check decoded JSON from file");
};

done_testing();
