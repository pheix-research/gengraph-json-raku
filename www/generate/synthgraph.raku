#!/usr/local/bin/raku

use v6.d;
use lib '/home/knarkhov/git/pheix-research/gengraph-json-raku/lib';
use Gengraph::JSON;

my %query;
my @pairs = %*ENV<QUERY_STRING>.split(/<[&;,]>/, :skip-empty);

for @pairs {
    my ($key, $value) = $_.split("=", :skip-empty);
    %query{$key} = $value;
}

my Hash $cfg = %(
     nodes         => %query<nodes>:exists && %query<nodes> > 1 ?? %query<nodes> !! 10,
     conditions    => %query<conditions>:exists && %query<conditions> > 0 ?? %query<conditions> !! 1,
     condchain_len => %query<condchainl>:exists && %query<condchainl> > 0 ?? %query<condchainl> !! 1,
     node_template => %(
         content => 1,
         type    => "f",
         args    => %()
     )
);

my $gg = Gengraph::JSON.new(:config($cfg));
my $nodes = $gg.generate_nodes;
my $edges = $gg.generate_edges(:nodes($nodes));

say 'Content-Type: application/json' ~ "\n";
# say $gg.json(%query);
say $gg.json(%(|$nodes, |$edges));
