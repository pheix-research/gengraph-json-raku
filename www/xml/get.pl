#!/wc/bin/env perl

use lib '/wc/lib';
use lib '/wc/components/pragmas/lib';

use strict;
use warnings;
use feature 'say';

use XML::Simple;
use LWP::UserAgent;
use Data::Dumper;
use Encode;
use CGI;

use constant WID => 1000005;

my $wid = $ARGV[0] // WID;

my $co = CGI->new();

if ($co->request_method && $co->request_method eq 'POST') {
    $wid = $co->param('wf_id') // WID;
}

my $message = qq`<?xml version="1.0" encoding="UTF-8"?>
    <request version="2.2.x">
        <clientinfo>
            <ipaddress>10.1.10.131</ipaddress>
            <useragent>Chrome/54.0.2840.99 Safari/537.36</useragent>
            <acceptlanguage>en-US,en;q=0.8,es;q=0.6</acceptlanguage>
            <passport>33ArLacoRiLTE6rSkumsQ7CBybc</passport>
            <application>MERCHANT:V1</application>
        </clientinfo>
        <function>
            <padcfgetworkflow>
                <w_id>$wid</w_id>
            </padcfgetworkflow>
        </function>
</request>
`;

my $webpage="http://127.0.0.1:8080/webcashier";
my $ua = LWP::UserAgent->new; 
my $response = $ua->post($webpage, Content_Type => 'text/xml', Content => $message);
if ($response->is_success) {
    #print $response->decoded_content;
    my $data = XMLin($response->decoded_content);
    print $co->header('application/json;charset=UTF-8');
    say encode('utf-8', $data->{function}{padcfgetworkflow}{workflow}{graph} // '{"error":"graph for workflow '.$wid.' is not found"}');
}
else {
    die $response->status_line;
}

