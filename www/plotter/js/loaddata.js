var sample_json = {}

$.getJSON('/plotter/upload.pl?' + Math.random(), function( json) {
    var colors = [window.chartColors.red, window.chartColors.blue, window.chartColors.purple];
    sample_json.labels = json.descriptions;
    sample_json.datasets = [];
    for( var i=0; i < json.labels.length; i++) {
        sample_json.datasets.push({label: json.labels[i], data: [], borderColor: colors[i], backgroundColor: colors[i], fill: false});
        for( var j=0; j < json.datasets.length; j++) {
            sample_json.datasets[i].data.push(json.datasets[j][i]);
        }
    }
    config.data = sample_json;
    var ctx = document.getElementById('canvas').getContext('2d');
    window.myLine = new Chart(ctx, config);
    console.log("chart");
});

