#!/usr/bin/perl

use strict;
use warnings;
use feature 'say';

use CGI;
use CGI::Carp qw(fatalsToBrowser);
use JSON::PP qw(encode_json);
use Data::Dumper;

my $plot;
my @datavalues;
my $fname     = './plot.txt';
my $json      = './plot.json';

my $co = CGI->new();

# router
if ($co->request_method eq 'POST') {
    my $txt = $co->param('bamboodata');

    if (defined $txt) {

        $txt =~ s/^([a-z\d\-\:\s]+\#[\s]+)//mgi;

        open my $f, '>', $fname or die 'unable to open file ' . $fname . ': ' . $!;
        print $f $txt;
        close $f or die 'unable to close file ' . $fname . ': ' . $!;

        my @lines = split /^/m, $txt;
        for my $l (@lines) {
            if ($l =~ /^[\d]+/) {
                my @values = split /[\s]+/m, $l;
                push @datavalues, \@values;
            }
        }
        if (@datavalues) {
            $plot = { labels => ['time', 'iters/sec', 'nodes/sec' ], descriptions => [], datasets => [] };
            my @sorted = sort { $a->[0] <=> $b->[0] } @datavalues;
            for my $values (@sorted) {
                push @{$plot->{descriptions}}, @{$values}[0];
                shift @{$values};
                push @{$plot->{datasets}}, \@{$values};
            }
	    open my $f, '>', $json or die 'unable to open file ' . $json . ': ' . $!;
            print $f encode_json($plot);
            close $f or die 'unable to close file ' . $json . ': ' . $!;
        }
    }

    print $co->redirect('https://' . $ENV{SERVER_NAME} . '/plotter/index.html');
}
else {
    open my $f, '<', $json or die 'unable to open file ' . $json . ': ' . $!;
    my $json_data = join q{}, <$f>;
    close $f or die 'unable to close file ' . $json . ': ' . $!;

    say $co->header();
    say $json_data;
    #say $co->start_html();
    #say Dumper($plot);
    #say $co->end_html();
}
