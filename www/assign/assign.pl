#!/wc/bin/env perl

use lib '/wc/lib';
use lib '/wc/components/pragmas/lib';

use strict;
use warnings;
use feature 'say';

use Data::Dumper;
use Encode;
use CGI;
use CGI::Carp qw(fatalsToBrowser);

use WebCashier::Libs;

use MSP::CF::System ();
use UBX::Request::Script ();
use Util::Common qw(Log);

use constant WID => 1000006;
use constant PO  => 12134;

my %sql = (
    update_padorder_po => q/
        UPDATE padorders
        SET    po_pw_id = ?
        WHERE  po_id = ?
    /,
    update_padorder_mt => q/
        UPDATE padorders
        SET    po_pw_id = ?
        WHERE  po_mt_id = ?
    /,
    select_padorder_by_mt => q/
        SELECT *
        FROM   padorders
        WHERE  po_mt_id = ?
    /,
);

my $wid = $ARGV[0] // WID;
my $po  = $ARGV[1] // PO;

my $co = CGI->new();

if ($co->request_method && $co->request_method eq 'POST') {
    $wid = $co->param('w_id') // undef;
    $po  = $co->param('po_id') // undef;
}

if (defined($wid) && defined($po)) {
    my $query;
    my $pad_order;

    my $UBR = UBX::Request::Script->new('/wc/conf/UBX.conf');
    my $dbh = $UBR->Database('webcashier');

    if ($po > 1000000) {
        my $hash   = $dbh->selectrow_hashref($sql{select_padorder_by_mt}, undef, $po);
        $pad_order = $hash->{po_id};
        $query     = $sql{update_padorder_mt};
    }
    else {
        $pad_order = $po;
        $query     = $sql{update_padorder_po};
    }

    my $sth = $dbh->prepare($query) || die $dbh->errstr;
    $sth->execute($wid, $po) || die $dbh->errstr;

    print $co->header, $co->start_html,
          "<p>Padorder <b>$pad_order</b> is updated!<br>Workflow is set to <b>$wid</b>.</p>",
          $co->end_html;
}
else {
    print $co->header, $co->start_html,
          "<p>Error while assigning workflow: ids should not be blank</p>",
          $co->end_html;
}

