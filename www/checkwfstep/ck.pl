#!/wc/bin/env perl

use lib '/wc/lib';
use lib '/wc/components/pragmas/lib';

use strict;
use warnings;
use feature 'say';

use JSON::PP;
use Data::Dumper;
use Encode;
use CGI;
use CGI::Carp qw(fatalsToBrowser);

use WebCashier::Libs;

use MSP::CF::System ();
use UBX::Request::Script ();
use Util::Common qw(Log);

use constant PO  => 12236;

my %sql = (
    select_history_po => q/
        SELECT history
        FROM   cf.trace
        WHERE  po_id = ?
    /,
);

my $po  = $ARGV[0] // PO;

my $co    = CGI->new;
my $coder = JSON::PP->new;

if ($co->request_method && $co->request_method eq 'GET') {
        $po  = $co->param('po_id') // undef;
}

if (defined($po)) {
    my $query;

    my $UBR = UBX::Request::Script->new('/wc/conf/UBX.conf');
    my $dbh = $UBR->Database('webcashier');

    my $t = $dbh->selectrow_hashref(sprintf("SELECT history FROM cf.trace WHERE po_id = %d;", $po));

    if (defined $t->{history}) {
        my $history_record = $coder->decode($t->{history});
        my $last_key       = (sort { $a <=> $b } grep {int $_} keys %{$history_record})[-1];
        my $et             = $history_record->{$last_key}{completed} == 0 ? (int(time) - int($history_record->{$last_key}{time})) : 'finished';
        my $info           = sprintf("workflow has run, last stop at node %02d: elapsed time=%s, completed=%d", $last_key, $et, $history_record->{$last_key}{completed});
        
        print $co->header, $co->start_html,
              "<p>Workflow for PAD order <b>$po</b> details: $info.</p><hr>",
              "<p>Execution trace:</p><textarea style=\"width:100%; height:600px\">",
              Dumper($history_record), "</textarea>",
              $co->end_html;
    }
    else {
        print $co->header, $co->start_html,
              "<p>Workflow for PAD order <b>$po</b> details: no history found.</p>",
              $co->end_html;
    }
}
else {
    print $co->header, $co->start_html,
          "<p>Error while accessing workflow history.</p>",
          $co->end_html;
}
